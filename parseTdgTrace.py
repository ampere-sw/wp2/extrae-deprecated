#this whole idea will not work for untied tasks
import re
import collections
import sys
import copy

#first input should be .prv file, second _tdg.c file and third .pcf file. 
#.prv contains the execution data measured by Extrae, _tdg.c contains the TDG structure and related info (like dependencies) gathered by Mercurium, 
#.pcf contains list of Extrae configuration flags, including papi event flag, so based on this we know which hardware counters have been collected
if(len(sys.argv)) < 4:
    quit()
prvInputFile = sys.argv[1]
tdgInputFile = sys.argv[2]
pcfInputFile = sys.argv[3]

if(not(prvInputFile.endswith(".prv")) or not(tdgInputFile.endswith("tdg.c")) or not(pcfInputFile.endswith(".pcf"))):
    print("\n Wrong input files. Expecting, in order: example.prv example_tdg.c example.pcf. Exiting. \n")
    quit()

#in the prv file the lines are events; the current time is captured for every event; 
#the time flows "downwards" meaning that events on the bottom of the file happened after events on the top of the file;
#5th number in each event is the thread id; this means that events must be grouped into events belonging to each thread, so that their timeline can be followed correctly;
#we are interested into events of task execution begin and task execution end, to know when tasks are executing;
#task execution is marked by flag 60000023, while task id itself is marked by flag 60000028; the latter appears only in events of task execution begin;
#for events of task execution end, flag 60000028 is not present, and flag 60000023 has zero value (first next number (to the right) beside the flag),
#this means that we have to track the timeline of each thread to know which task has ended in the given event (the task that started the last), 
#since the task execution end event data itself does not show that
lines = []
with open(prvInputFile) as prvInput:
    lines = prvInput.readlines()
papiEventLines = ""
with open(pcfInputFile) as pcfInput:
    papiEventLines = pcfInput.read()

papiEventsFlagList = re.findall(r'42[0-9]{6}', papiEventLines) #the flag for the hardware event counters is always 42 mln something

#exampe for papi event flags, from the pcf
#EVENT_TYPE
#7  42000050 PAPI_TOT_INS [Instr completed]
#7  42000059 PAPI_TOT_CYC [Total cycles]
#7  42000000 PAPI_L1_DCM [L1D cache misses]
#7  42000002 PAPI_L2_DCM [L2D cache misses]
#7  42000008 PAPI_L3_TCM [L3 cache misses]
#7  42000055 PAPI_BR_INS [Branches]
#7  42000046 PAPI_BR_MSP [Cond br mspredictd]
#7  42001047 RESOURCE_STALLS [Cycles Allocation is stalled due to Resource Related reason]
#=>
#  papiEventFlags = ['42000050', '42000059', '42000000', '42000002', '42000008', '42000055', '42000046', '42001047']

papiEventFlags = papiEventsFlagList
capturingEventTimeFlag = '60000023' #task execution flag, positioned just behind the execution time
capturingEventIdFlag = '60000028' #task id flag, which will have the value of the ID in the first following prv field
captureEventFlags = [capturingEventIdFlag, capturingEventTimeFlag] + papiEventFlags
captureEventValues = {} #are the values for the flags in captureEventFlags, but mapped per thread
numOfPapiEvents = len(papiEventFlags)
numOfValuesToCapture = len(captureEventFlags)  #event id, event time, all papi event counters in the current prv
captureEventIdIndx = 0 #for the ease of reading
captureEventTimeIndx = 1
allCapturesCollection = [] #the final result of the first part of the script (parsing extrae outputs)

def initCountersIfNeeded(captureEventValues, thrId):
    if(thrId not in captureEventValues):
        captureEventValues[thrId] = [0] * numOfValuesToCapture 

def resetCounters(captureEventValues, thrId):
    if(thrId in captureEventValues):
        captureEventValues[thrId] = [0] * numOfValuesToCapture
    else:
        print("Error! Counters should already be initialized for this thread, before reset.")
        quit()

#PAPI counters show the sum of all event counts, since the last event execution,
#which means it is enough to just add them up in the timeframe that we need (like during a capture event)
def incrementCounterValuesForEventInThread(captureEventValues, thrId, entry):
    papiEventIndx = 0 #since the captureEvents have 2 more values than papiEvents, need to add it to the papiEventIndx below
    while papiEventIndx < len(papiEventFlags):
        papiEvent = papiEventFlags[papiEventIndx] #the trick here is that papi event flags (in papiEventFlags list) are ordered exactly in the order we will collect the data for them
        #this means that papiEventIndx will be the same index for the list of flags as for the list of values that belong to these papi event flags
        indx = 0
        while(indx < len(entry)):
            #the value of the papi counter is always located right after the papi event id in the prv entry (prv line):
            if(papiEvent == entry[indx]):
                #and we want to add up all others, because they measure sum of events in a given timeframe (since the last measurement)
                captureEventValues[thrId][2 + papiEventIndx] = captureEventValues[thrId][2 + papiEventIndx] + int(entry[indx+1])
                break; #we need only the first match, cause we are interested only in first papi event val, that is in Next Evt Val
            indx += 1
        papiEventIndx += 1
    #print(captureEventValues[thrId])

def isCaptureEventStart(entry):
    if((capturingEventIdFlag in entry) and (capturingEventTimeFlag in entry)):
        return True
    else:
        return False

def isCaptureEventEnd(entry):
    if( (capturingEventTimeFlag in entry) and not (capturingEventIdFlag in entry) and (entry[entry.index(capturingEventTimeFlag)+1] == '0') ):
        return True
    else:
        return False

#assuming that this is correctly recognized capture start event (in the current case: capture event is task execution event 60000023, but this can be changed if needed)
def startEventCapture(captureEventValues, thrId, entry):
    #print("StartCapture: " +  entry[entry.index(capturingEventIdFlag) + 1] + "  time:" + entry[entry.index(capturingEventTimeFlag) - 1])
    #the captureEventValues is like a running counter of all values that we follow,
    #but in-between capture events per thread, this should not be marked to correspond to any capture event (by first 2 fields),
    #because, when parsing prv events (lines) that are not related to task, we are not sure whether to capture the papi values in those or not, 
    #so we do it always, but assign (memorize them) only if we are collecting for the specific task (in processOtherEvents)
    if((thrId in captureEventValues) and (captureEventValues[thrId][0] != 0) and (captureEventValues[thrId][1] != 0)):
        print("Some miscalculation has happened and ending of previous capture event has not reset the counters. Aborting...")
        quit()
    resetCounters(captureEventValues, thrId) #still have to do this because of PAPI events in-between two capture events we are interested in
    captureEventValues[thrId][captureEventIdIndx] = entry[entry.index(capturingEventIdFlag) + 1] #this value is actually the ID we need  
     #we will assume that the time is always located one place before the event time flag, because that is how prv is structured 
    captureEventValues[thrId][captureEventTimeIndx] = int(entry[entry.index(capturingEventTimeFlag) - 1])

#assuming that this is correctly recognized end event
def endEventCapture(captureEventValues, thrId, entry):
    #print("endCapt time:" + entry[entry.index(capturingEventTimeFlag) - 1])
    #do nothing for captureEventValues[thrId][0]
    captureEventValues[thrId][captureEventTimeIndx] = int(entry[entry.index(capturingEventTimeFlag) - 1]) - captureEventValues[thrId][captureEventTimeIndx]  #calculate execution time
    incrementCounterValuesForEventInThread(captureEventValues, thrId, entry)  #increment all PAPI event counters, so that we get final sum for this capture event
    allCapturesCollection.append(captureEventValues[thrId])
    resetCounters(captureEventValues, thrId) #finished mapping counters on this thread in this timeframe to this capture event

#we need to add up all PAPI counters during the execution time of an event
def processOtherEvents(captureEventValues, thrId, entry):
    incrementCounterValuesForEventInThread(captureEventValues, thrId, entry)


entry = []
for l in lines:
    entry = l.strip().split(':')
    thrId = int(entry[4])
    initCountersIfNeeded(captureEventValues, thrId)
    if(isCaptureEventStart(entry)):
        startEventCapture(captureEventValues, thrId, entry)
    elif(isCaptureEventEnd(entry)):
         endEventCapture(captureEventValues, thrId, entry)
    else:
        processOtherEvents(captureEventValues, thrId, entry)

        
##############################################################################################        
#now let's parse the tdg.c file, so we can see which data to modify with the execution times
tdgLines = ''
tdgStruct =''
with open(tdgInputFile) as tdgInput:
    tdgLines = tdgInput.read()

tdgOutputLines = tdgLines

nameOfExecutionTime = "execution_time"
#the PAPI flags and values list will have identically ordered elements, which means that first element of the value list corresponds to the first element of the flags list
nameOfPapiFlags = "papitot_counter_flags"
nameOfPapiVals = "papitot_counter_vals"
#small cleanup of the tdg values that we don't need:
def isMemberToRemove(member):
    if(("task_counter" in member) or ("task_counter_end" in member) or ("runtime_counter" in member) or ("taskpart_counter" in member) or ("map" in member) or ("cnt" in member) or ("next_waiting_tdg" in member)):
        return True
    else:
        return False

#parse the tdg structure (taking into account that we might have more than one tdg structures, but in that case, the tdg task ids MUST be different between the TDGs somehow)
#get the content of the body of the tdg strcut declaration
matchTdgStructDecl = re.search(r'struct[\s]*gomp_tdg[\s]*\{([\s\S]*?)\}[\s]*;', tdgLines)
tdgStructDeclBody = matchTdgStructDecl.group(1)
tdgStructDeclBodyNew = copy.copy(tdgStructDeclBody) + "\n\n\t int " + nameOfExecutionTime + "; \n\t int " + nameOfPapiVals + "[" + str(numOfPapiEvents) + "]; \n\n"

tdgStructDeclBodyNewLines = tdgStructDeclBodyNew.splitlines() #asuming (hoping) that member declarations are in separate lines
for member in tdgStructDeclBodyNew.splitlines():
     if(isMemberToRemove(member)):
        tdgStructDeclBodyNewLines.remove(member)

tdgStructDeclBodyNew = '\n'.join(tdgStructDeclBodyNewLines)
tdgStructDeclWhole = matchTdgStructDecl.group(0)

tdgStructDeclWholeNew = tdgStructDeclWhole.replace(tdgStructDeclBody, tdgStructDeclBodyNew)
tdgStructDeclWholeNewPlusPapiFlagDecl = tdgStructDeclWholeNew + "\n\n" + "int " + nameOfPapiFlags + "[" + str(numOfPapiEvents) + "] = {" + ','.join(papiEventFlags) + "}; \n\n"

tdgOutputLines = tdgOutputLines.replace(tdgStructDeclWhole, tdgStructDeclWholeNewPlusPapiFlagDecl) 
tdgFromScratchOutputLines = "\n" + tdgStructDeclWholeNewPlusPapiFlagDecl + "\n"

listAllTdgStructs = re.findall(r'struct[\s]*gomp_tdg[\s]*gomp_tdg_[0-9]+\[.*?\][\s]*=[\s]*\{[\s\S]*?\}[\s]*;', tdgLines)
numOfTdgs = len(listAllTdgStructs)
for tdgStructInList in listAllTdgStructs:
    
    #get the content of the body of the tdg struct definition
    matchTdgStruct = re.search(r'struct[\s]*gomp_tdg[\s]*gomp_tdg_[0-9]+\[.*?\][\s]*=[\s]*\{([\s\S]*?)\}[\s]*;', tdgStructInList)   
    tdgStruct = matchTdgStruct.group(1)
    
    #parse it into dictionary with keys as task ids and values as the list of vars describing that node
    oldTdg = {}
    tdgNode = []
    tdg = {}
    for matchTdgNode in re.finditer(r'\{(.*?)\}', tdgStruct):
        tdgNode = matchTdgNode.group(1).split(',')
        matchId = re.search(r'\.id.*?(\d+)', tdgNode[0]) #expecting/asuming that id is the first member of the tdg node struct
        tdgId = int(matchId.group(1))
        oldTdg[tdgId] = tdgNode 
        #items to remove: task, task_counter, task_counter_end, runtime_counter, taskpart_counter, cnt, map,   
        tdg[tdgId] = copy.copy(tdgNode)
        for member in tdgNode:
            if(isMemberToRemove(member)):
                tdg[tdgId].remove(member) #because tdg has been created based on tdgNode, it must have the same members
        for capture in allCapturesCollection:
            if(capture[captureEventIdIndx].strip() == str(tdgId)):
                tdg[tdgId].append("." + nameOfExecutionTime + " = " + str(capture[captureEventTimeIndx]))
               # tdg[tdgId].append(".papitot_ns_flags = {" + ','.join(papiEventFlags) + "}") #now added as a separate declaration, outside of this struct
                tdg[tdgId].append("." + nameOfPapiVals + " = {" + ','.join(list(map(str,capture[2:]))) + "}")
        #print(tdg[tdgId])
  
    
    #bring it all together into output tdg.c file
    tdgForWrite = []
    tdgCombinedLines = tdgStructInList
    for node in tdg:
        if node not in oldTdg:
            print("Old and new tdgs must have same nodes!")
            quit()
        tdgCombinedLines = tdgCombinedLines.replace("{" + ','.join(oldTdg[node]) + "}", "{" + ','.join(tdg[node]) + "}")
        
    #dependencies parse:
    matchTdgStructDependIns = re.findall(r'unsigned.*?gomp_tdg_ins[\s\S]*?;', tdgLines)  
    matchTdgStructDependOuts = re.findall(r'unsigned.*?gomp_tdg_outs[\s\S]*?;', tdgLines)
    tdgLinesDependInsAndOuts = '\n' + '\n'.join(matchTdgStructDependIns) + '\n' + '\n'.join(matchTdgStructDependOuts) + '\n'
    #end of dependencies parse
    tdgOutputLines = tdgOutputLines.replace(tdgStructInList, tdgCombinedLines)
    tdgOutputLines = re.sub(r'struct[\s]*gomp_tdg\s', 'struct gomp_tdg_node ', tdgOutputLines)   
    tdgOutputLines = re.sub(r'struct[\s]*gomp_tdg\{', 'struct gomp_tdg_node{', tdgOutputLines)   
    tdgOutputLines = re.sub(r'struct[\s]*gomp_tdg;', 'struct gomp_tdg_node;', tdgOutputLines)   
    
    tdgFromScratchOutputLines = tdgFromScratchOutputLines + "\n\n" + tdgCombinedLines
    tdgFromScratchOutputLines = re.sub(r'struct[\s]*gomp_tdg\s', 'struct gomp_tdg_node ', tdgFromScratchOutputLines)   
    tdgFromScratchOutputLines = re.sub(r'struct[\s]*gomp_tdg\{', 'struct gomp_tdg_node{', tdgFromScratchOutputLines)   
    tdgFromScratchOutputLines = re.sub(r'struct[\s]*gomp_tdg;', 'struct gomp_tdg_node;', tdgFromScratchOutputLines)   
    tdgFromScratchOutputLines = tdgFromScratchOutputLines + '\n' + tdgLinesDependInsAndOuts
    

indx = tdgInputFile.index("tdg.c")

#tdgOutputFile = tdgInputFile[:indx] + "full_output_" + tdgInputFile[indx:]
#tdgOutput = open(tdgOutputFile, "w+t")
#tdgOutput.write(tdgOutputLines)
#tdgOutput.close()

indx2 = tdgInputFile.index("tdg.c")
tdgOutputFile2 = tdgInputFile[:indx] + "simple_output_" + tdgInputFile[indx:]
tdgOutput2 = open(tdgOutputFile2, "w+t")
tdgOutput2.write(tdgFromScratchOutputLines)
tdgOutput2.close()

